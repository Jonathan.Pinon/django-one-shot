from django.contrib import admin
from todos.models import TodoList, TodoItem


# Register your models here.
@admin.register(TodoList)
class TodoListadmin(admin.ModelAdmin):
    list_display = [
        "name",
        "id",
    ]


@admin.register(TodoItem)
class TodoItemadmin(admin.ModelAdmin):
    list_display = [
        "task",
        "due_date",
    ]
